from django.urls import path
from todos.views import todos_list, todo_list_detail, show_model_name

urlpatterns = [
    # path("pathway"), view, name="path_name"
    path("", todos_list, name="todo_list_list"),
    path("todos/<int:id>/", show_model_name, name="todo_list_detail"),
]
