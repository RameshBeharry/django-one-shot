from django.shortcuts import render, get_object_or_404
from todos.models import TodoList, TodoItem

# Create your views here.


def todos_list(request):
    todos = TodoList.objects.all()
    context = {"todos_list": todos}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    context = {
        "todo_object": todo_item,
    }

    return render(request, "todos/detail.html", context)


def show_model_name(request, id):
    todo_list_item = TodoList.objects.get(id=id)
    context = {"todo_list_item": todo_list_item}
    return render(request, "todos/detail.html", context)
